package br.com.mastertech.imersivo.filmesdb.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.DiretorDTO;
import br.com.mastertech.imersivo.filmesdb.model.FilmeDTO;
import br.com.mastertech.imersivo.filmesdb.repository.DiretorRepository;

@Service
public class DiretorService {
	
	@Autowired
	private DiretorRepository diretorRepository;
	
	@Autowired
	private FilmeService filmeService;
	
	public Iterable<Diretor> obterDiretores() {
		return diretorRepository.findAll();
	}
	
	public void criarDiretor(Diretor diretor) {
		diretorRepository.save(diretor);
	}

	public void apagarDiretor(Long id) {
		Diretor diretor = encontraOuDaErro(id);
		diretorRepository.delete(diretor);
	}
	
	public Diretor encontraOuDaErro(Long id) {
		Optional<Diretor> optional = diretorRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Filme não encontrado");
		}
		
		return optional.get();
	}

	public Iterable<FilmeDTO> obterFilmes(Long diretorId) {
		return filmeService.obterFilmes(diretorId);
	}
	
	public DiretorDTO obterDiretor(Long id) {
		return diretorRepository.findSummary(id);
	}

}
